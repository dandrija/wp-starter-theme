<?php 

/*
 * Single Post
 */

?>

<?php get_header(); ?>

<section class="content-area">
    <main>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'single' );?>

		<?php endwhile; // End of the loop. ?>

	</main>

    <?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>