<?php
/*
    Template Name: Full Width
 */

get_header(); ?>

<section class="content">

    <main>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php get_template_part( 'template-parts/content', 'page' ); ?>

		<?php endwhile; // End of the loop. ?>

    </main>

</section>

<?php get_footer(); ?>

