<!doctype html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, width=device-width">
	<meta name="format-detection" content="telephone=no">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
	 	var siteurl  = '<?php bloginfo( 'url' ); ?>/';
        var ajax_url = '<?php echo admin_url( 'admin-ajax.php' ) ?>';
	</script>

    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
	<header id="header" class="">
		<div class="header-inner">
			<button type="button" class="Navtoggle">
			    <span class="Navtoggle__icon">
			        <span class="Navtoggle__bar"></span>
			        <!-- <span class="menuicon__bar"></span> -->
			        <span class="Navtoggle__bar"></span>
			    </span>
			</button><!-- /.navtoggle -->

			<?php get_template_part( 'template-parts/navigation', 'main' ); ?>
		</div>
	</header><!-- /#header -->