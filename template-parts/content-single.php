<article class="Post">

	<h1 class="Post__title">
		<?php the_title(); ?>
	</h1>

	<div class="Post__content">
		<?php the_content(); ?>
	</div>

</article>