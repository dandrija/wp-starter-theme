<article class="Post">

	<h2 class="Post__title">
		<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
	</h2>

	<div class="Post__excerpt">
		<?php echo get_the_excerpt(); ?>
	</div>

</article>