<div class="Logo">
	<a class="Logo__link" href="<?php echo bloginfo('url'); ?>" title="<?php echo bloginfo('info'); ?>">
		<?php echo bloginfo('info'); ?>
	</a>
</div>

<nav id="navigation-main">
	<?php

		/**
		* Displays a navigation menu.
		*
		* @since 3.0.0
		*/
		$args = array(
			'theme_location' => 'primary',
			'menu' => '',
			'container' => '',
			'container_class' => '',
			'container_id' => '',
			'menu_class' => 'menu',
			'menu_id' => '',
			'echo' => true,
			'fallback_cb' => 'wp_page_menu',
			'before' => '',
			'after' => '',
			'link_before' => '',
			'link_after' => '',
			'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth' => 0,
			'walker' => ''
		);

		wp_nav_menu( $args );

	?>
</nav>