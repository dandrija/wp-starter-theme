<article class="Page">

	<h1 class="Page__title">
		<?php the_title(); ?>
	</h1>

	<div class="Page__content">
		<?php the_content(); ?>
	</div>

</article>