<?php 
	
	function add_button() {  
	   if ( current_user_can('edit_posts') &&  current_user_can('edit_pages')
	   		&& get_user_option('rich_editing')  )  
	   {  
	     add_filter('mce_external_plugins', 'add_plugin');  
		 add_filter('mce_buttons', 'register_button');  
	  }  
	}  

    function register_button($buttons) {
       //array_push($buttons, "|");
       //array_push($buttons, "plugin_name");
       return $buttons;  
    }  

    function add_plugin($plugin_array) {  
       //$plugin_array['plugin_name'] 					= 	get_bloginfo('template_url').'/js/customcodes.js';
	   return $plugin_array;  
    }  
	add_action('init', 'add_button');  

?>