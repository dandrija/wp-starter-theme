<?php

    
    // get global roles
    global $wp_roles;

    // if is empty - load
    if (!isset($wp_roles)){
        $wp_roles = new WP_Roles();
    }
    
    // get admin role
    $auth = $wp_roles->get_role('administrator');
    
    // exclude capabilities
    
    $exclude = array(
    
                    'edit_users',           /* user can't edit users */
                    'delete_users',         /* user can't delete users */
                    'create_users',         /* user can't create users */
                    
                    // 'activate_plugins',     /* user can't activate plugins */
                    'edit_plugins',         /* user can't edit plugins */
                    'update_plugins',       /* user can't update plugins */
                    'delete_plugins',       /* user can't delete plugins */
                    'install_plugins',      /* user can't install plugins */
                    
                    
                    'switch_themes',        /* user can't switch themes */
                    'edit_themes',          /* user can't edit themes */
                    'update_themes',        /* user can't update themes */
                    'install_themes',       /* user can't install themes */
                    'delete_themes',        /* user can't delete themes */
                    
                    'update_core',          /* user can't update WordPress core */
                    
                    'manage_options',       /* user can't manage options */
                    
                    'list_users',           /* user can't view users */
                    'remove_users',         /* user can't remove users */
                    'add_users',            /* user can't add users  */
                    'promote_users',        /* user can't promote users */
                    
                        // 'edit_theme_options',   /* user can't edit theme options */    
                    
                    'export',               /* user can export (tools) */
                    'import'                /* user can import (tools) */
                    
                );
     
    
    // copy capabilities
    $new_capabilities = $auth->capabilities;
    
    // cleat capabilities
    foreach( $exclude as $cap ){
        if( isset($new_capabilities[$cap]) ){
            unset($new_capabilities[$cap]);
        }
    }
    
    // select role alias
    // remove_role( CUSTOM_ROLE_ALIAS );
    $wp_roles->add_role( CUSTOM_ROLE_ALIAS , 'Website administrator', $new_capabilities);
    

    
    // if user role == $role_alias , then remove all pages from Appearance except Widgets and Menus
    $current_logged_user_data = wp_get_current_user();
    if( isset($current_logged_user_data->roles[0])
        &&
        ($current_logged_user_data->roles[0]) == CUSTOM_ROLE_ALIAS ){

            function hide_menu() {
    
                global $submenu;
                
                if( isset($submenu['plugins.php']) ){ unset($submenu['plugins.php']); }
            
                if( isset($submenu['themes.php']) 
                    && is_array($submenu['themes.php']) 
                    && count($submenu['themes.php'])){
                        
                        foreach( $submenu['themes.php'] as $num => $options ){
                            
                            if( isset($options[1])
                                &&
                                $options[1] == 'customize' ){
                                    
                                    unset($submenu['themes.php'][$num]);
                                    
                                }
                               
                        }
                        
                    }
    
                remove_submenu_page( 'themes.php', 'themes.php' ); // hide the theme selection submenu
            
                // these are theme-specific. Can have other names or simply not exist in your current theme.
                remove_submenu_page( 'themes.php', 'yiw_panel' );
                remove_submenu_page( 'themes.php', 'custom-header' );
                remove_submenu_page( 'themes.php', 'custom-background' );
                
                remove_menu_page( 'plugins.php' ); 


               
    
            }
                
        //add_action('admin_head', 'hide_menu', 99999);
        add_action('admin_menu', 'hide_menu', 99999);
            
    }
    
      
    //add_action( 'after_setup_theme','remove_theme_caps', 999  );
    add_action( 'admin_menu','remove_theme_caps', 999  );
    function remove_theme_caps() {
        
        
        $current_logged_user_data = wp_get_current_user();        

        if( isset($current_logged_user_data->roles[0])
            &&
            ($current_logged_user_data->roles[0]) == CUSTOM_ROLE_ALIAS ){
                
                global $submenu;
                if( isset($submenu['plugins.php']) ){ unset($submenu['plugins.php']); }
                
                remove_theme_support( 'custom-background' );
                remove_theme_support( 'custom-header' );
                // remove_theme_support( 'menus' );
                
                
            }
            

    }
  


?>