<?php


/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 584;

/**
 * Tell WordPress to run paramount() when the 'after_setup_theme' hook is run.
 */
add_action( 'after_setup_theme', 'paramount' );


// theme setup
if ( ! function_exists( 'paramount' ) ){
	function paramount() {


		// post templates
		// add_filter('single_template', create_function('$t', 'foreach( (array) get_the_category() as $cat ) { if ( file_exists(TEMPLATEPATH . "/single-{$cat->slug}.php") ) return TEMPLATEPATH . "/single-{$cat->slug}.php"; } return $t;' ));

		// This theme styles the visual editor with editor-style.css to match the theme style.
		add_editor_style();

		// Load custom functions
		if( file_exists( get_template_directory() . '/inc/custom-functions.php' ) ){
			require( get_template_directory() . '/inc/custom-functions.php' );
		}

		// Load TGM
		if( file_exists( get_template_directory() . '/inc/tgm-loader.php' ) ){
			require( get_template_directory() . '/inc/tgm-loader.php' );
		}

		// ACF Import
		if( file_exists( get_template_directory() . '/inc/acf-import.php' ) ){
			require( get_template_directory() . '/inc/acf-import.php' );
		}


		// Load custom header
		if( file_exists( get_template_directory() . '/inc/custom-header-support.php' ) ){
			require( get_template_directory() . '/inc/custom-header-support.php' );
		}

		// Load custom background
		if( file_exists( get_template_directory() . '/inc/custom-background.php' ) ){
			require( get_template_directory() . '/inc/custom-background.php' );
		}


		// Load up our theme options page and related code.
		if( file_exists( get_template_directory() . '/inc/theme-options.php' ) ){
			require( get_template_directory() . '/inc/theme-options.php' );
		}

		// Load custom thumb sizes
		if( file_exists( get_template_directory() . '/inc/custom-thumb-sizes.php' ) ){
			require( get_template_directory() . '/inc/custom-thumb-sizes.php' );
		}

		// Load sidebars
		if( file_exists( get_template_directory() . '/inc/sidebars.php' ) ){
			require( get_template_directory() . '/inc/sidebars.php' );
		}

		// Load TinyMCE buttons
		if( file_exists( get_template_directory() . '/inc/tinymce_buttons.php' ) ){
			require( get_template_directory() . '/inc/tinymce_buttons.php' );
		}

		// Load shortcodes
		if( file_exists( get_template_directory() . '/inc/shortcodes.php' ) ){
			require( get_template_directory() . '/inc/shortcodes.php' );
		}


		// Grab widgets.
		if( file_exists( get_template_directory() . '/inc/widgets.php' ) ){
			require( get_template_directory() . '/inc/widgets.php' );
		}

		// Custom theme admin page.
		if( file_exists( get_template_directory() . '/inc/theme-admin.php' ) ){
			require( get_template_directory() . '/inc/theme-admin.php' );
		}

		// Custom  user roles
	    define("CUSTOM_ROLE_ALIAS", 'website_admin');
		if( file_exists( get_stylesheet_directory() . '/inc/custom-user-roles.php' ) ){
	        require( get_stylesheet_directory() . '/inc/custom-user-roles.php' );
		}
	}

	// Add default posts and comments RSS feed links to <head>.
	add_theme_support( 'automatic-feed-links' );

	// This menus
	register_nav_menu( 'primary', __( 'Primary Menu', 'paramount' ) );

    // title tag
    add_theme_support( 'title-tag' );

	// load javascript
	function load_jquery_scripts() {

        wp_enqueue_style( 'style',  get_stylesheet_directory_uri()  . '/style.css');

		// register default scripts
        wp_register_script( 'TweenMax',  get_stylesheet_directory_uri()  . '/js/TweenMax.min.js');
        wp_register_script( 'gsap',  get_stylesheet_directory_uri()  . '/js/jquery.gsap.min.js');

		wp_register_script( 'onload',  get_stylesheet_directory_uri()  . '/js/onload.js', '', '', true);

		// load
		wp_enqueue_script('jquery');
        wp_enqueue_script('TweenMax');
        wp_enqueue_script('gsap');
	    wp_enqueue_script('onload');

	}
	add_action('wp_enqueue_scripts', 'load_jquery_scripts');
}

?>