<?php get_header(); ?>

<section class="content">
    <main>

        <?php if ( have_posts() ) : ?>

            <?php while ( have_posts() ) : ?>
                <?php the_post(); ?>

                <?php get_template_part( 'template-parts/content', get_post_format() ); ?>

            <?php endwhile; ?>

        <?php else : ?>

            <?php get_template_part( 'template-parts/content', 'none' ); ?>

        <?php endif; ?>

    </main>

    <?php get_sidebar(); ?>
</section>

<?php get_footer(); ?>