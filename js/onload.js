(function($){
    $(document).ready(function(){

	    var toggle_button = $('.Navtoggle');
	    var navigation = $('#navigation-main');

		//////////////////
	    // Menu Toggler //
	    //////////////////


	    toggle_button.bind('click', function() {
	        navigation.slideToggle('400');

	        $(this).toggleClass('Navtoggle--open');

	        return false;
	    });

        $(window).resize(function(){
            if( $(window).width() > 768) {
	        	navigation.slideUp('400');
	        	toggle_button.removeClass('Navtoggle--open');
            }
        });

    });
})(jQuery);