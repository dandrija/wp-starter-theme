<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package Paramount Theme
 */

get_header(); ?>

<section class="content error-404 not-found">
    <main>

    	<article class="Page">
			<h1 class="Page__title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'paramount' ); ?></h1>

			<div class="Page__content">
				<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'paramount' ); ?></p>
			</div><!-- .page-content -->
		</article>

    </main>
</section>

<?php get_footer(); ?>
